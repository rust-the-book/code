fn main() {
    let mut s = String::from("hello world");

    let word = first_word(&s);

    s.clear();

    let s = String::from("hello world");
    let s1 = &s[0..5];
    let s2 = &s[6..11];
    let s3 = &s[..5];
    let s4 = &s[6..];
    let s6 = &s[..];

    let s = String::from("hello world");
    let word = first_word_slice(&s);

    // s.clear();

    println!("1st word is {}", word);

    let s = String::from("hello world");
    let word = first_word_slice_all_way(&s);
    let word = first_word_slice_all_way(&s[..]);
    let word = first_word_slice_all_way(&s[0..s.len()]);

    let s = "hello world";
    let word = first_word_slice_all_way(&s);
    let word = first_word_slice_all_way(&s[..]);
    let word = first_word_slice_all_way(&s[0..s.len()]);

    let a = [1, 2, 3, 4, 5];
    let a1 = &a[..];
    let a2 = &a[0..];
    let a3 = &a[..a.len()];
}

fn first_word(s: &String) -> usize {
    let bytes = s.as_bytes();

    for (i, &item) in bytes.iter().enumerate() {
        if item == b' ' {
            return i;
        } 
    }

    s.len()
}

fn first_word_slice(s: &String) -> &str {
    let bytes = s.as_bytes();

    for (i, &item) in bytes.iter().enumerate() {
        if item == b' ' {
            return &s[..i]
        } 
    }

    &s[..]
}

fn first_word_slice_all_way(s: &str) -> &str {
    let bytes = s.as_bytes();

    for (i, &item) in bytes.iter().enumerate() {
        if item == b' ' {
            return &s[..i]
        } 
    }

    &s[..]}
