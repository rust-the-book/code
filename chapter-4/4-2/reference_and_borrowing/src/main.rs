fn main() {
    let s = String::from("hello");

    let (s, len) = calculate_length(s);

    println!("length is {}", len);

    let s = String::from("hello");

    let len = calculate_length_reference(&s);

    println!("length is {}", len);

    let s = String::from("hello");

    change(&s);


    let mut s = String::from("hello");

    change_mutable(&mut s);

    println!("s is {}", s);

    let mut s = String::from("hello");
    let r1 = &s;
    let r2 = &s;

    println!("r1 {}, r2 {}", r1, r2);

    let r3 = &mut s;

    r3.push_str(", world!");

    println!("r3 {}", r3);

    let points_to_nothing = dangling_reference();
}

fn calculate_length_reference(s: &String) -> usize {
    s.len()
}

fn calculate_length(s: String) -> (String, usize) {
    let length = s.len();

    (s, length)
}

fn change(s: &String) {
    // s.push_str(", world!");
}

fn change_mutable(s: &mut String) {
    s.push_str(", world!");
}

fn dangling_reference() -> &String {
    let s = String::from("hello");

    &s
}
