use std::fs::{self, File};
use std::io::{self, ErrorKind, Read};
use std::error::Error;

// main by default returns `()`
// `Box<dyn Error>` - trait object
// like catch all error
fn main() -> Result<(), Box<dyn Error>> {
    // this will throw an error
    // `?` can be used in function that returns `Result`, `Option` or
    // any type that implements `Try` trait
    let f = File::open("hello.txt")?;
    Ok(())
    /*
    let f = match f {
        Ok(f) => f,
        Err(err) => panic!("Problem opening file: {:#?}", err)
    };*/

    // lets handle it better
    // more match! more verbose!
    /*
    let f = match f {
        Ok(f) => f,
        Err(err) => match err.kind() {
            ErrorKind::NotFound => match File::create("hello.txt") {
                Ok(f) => f,
                Err(err) => panic!("Unable to create file: {:#?}", err)
            },
            other_err => panic!("Unable to read file: {:#?}", other_err)
        }
    };*/

    // lets handle it even better, rustacean style!
    /*
    let f = File::open("hello.txt").unwrap_or_else(|error| {
        if error.kind() == ErrorKind::NotFound {
            File::create("hello.txt").unwrap_or_else(|error| {
                panic!("Unable to create file: {:#?}", error);
            })
        } else {
            panic!("Unable to read file: {:#?}", error);
        }
    });
    */

    // similar to `match` & `panic!`, default error message
    // let f = File::open("hello.txt").unwrap();
    // similar to `unwrap`, takes error messge
    // let f = File::open("hello.txt").expect("Unable to read file");
}

// good to let calling code handle errors
fn read_username_from_file() -> Result<String, io::Error> {
    let mut f = match File::open("username.txt") {
        Ok(f) => f,
        Err(err) => return Err(err)
    };
    let mut s = String::new();
    
    match f.read_to_string(&mut s) {
        Ok(_) => Ok(s),
        Err(err) => Err(err)
    }
}

fn read_username_from_file_1() -> Result<String, io::Error> {
    let mut s = String::new();
    // `?` removes boilerplate
    // return `File` if `Ok` else return `Err`
    // `?` goes through `from` function defined in `From` trait
    // converts one Error to another
    // like error from `File::open` to `io::Error`
    let mut f = File::open("username.txt")?;
    f.read_to_string(&mut s)?;

    // shorter, rustacean style
    File::open("username.txt")?.read_to_string(&mut s)?;

    Ok(s)
}

// even shorter!
fn read_username_from_file_2() -> Result<String, io::Error> {
    // creates mutable string
    // read from file
    // return `String` if `Ok` else `Err`
    fs::read_to_string("username.txt")
}
