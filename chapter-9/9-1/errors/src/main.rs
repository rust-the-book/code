fn main() {
    // rust - print failure, unwind & clean up stack
    // to abort (without cleanup) - set `panic = 'abort'`
    // in Cargo.tom `[profile.release]` section
    // panic!("Crash and Burn!");

    let v = vec![1, 2, 3];

    // panic!
    // use RUST_BACKTRACE=1 to print stacktrace
    // backtrace - list of functions called leading to panic
    // debug symbols needed! default in cargo run & build
    v[99];
}
