mod front_of_house {
    pub mod hosting {
        pub fn add_to_waitlist() {}
    }
}

use crate::front_of_house::hosting;
// also valid!
// use front_of_house::hosting; 

// valid, but not idiomatic
// obscures the fact that `add_to_waitlist` isn't a local one
// use frant_of_house::hosting::add_to_waitlist;

// ok for struct and enum
use std::collections::HashMap;

// not ok when two modules expose same type
// like io::Result and fmt::Result
use std::io;
use std::fmt;

// `as` differentiates between the two Result
use std::io::Result as IOResult;
use std::fmt::Result;

// allows external code to use `r_hosting`
// good way to maintain one structure internally, but expose another for external code to use
// like customers wont' understand `front_of_house` and `back_of_house`
pub use crate::front_of_house::hosting as r_hosting;

// nested paths clear clutter
// similar to python's `from module import a,b`
use std::fs::{self, File};

// bring in all public items to scope
// obscures which item came from where!
// useful in tests though
use std::collections::*;

pub fn eat_at_restaurant() {
    hosting::add_to_waitlist();
    hosting::add_to_waitlist();
    hosting::add_to_waitlist();
}
