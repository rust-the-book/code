// any file under tests is executed, even if no tests
// create a module this way - tests/common/mod.rs
// useful for sharing functionality with multiple files
mod common;

use adder;

#[test]
fn it_adds_two() {
    common::setup();

    assert_eq!(4, adder::add_two(2));
}
