// cfg -> configuration
// rust includes following item based on config option provided
// ex: cargo test
// only compiled with `cargo test` not `cargo build`
#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn it_adds_two() {
        assert_eq!(4, add_two(2));
    }

    // because mod rules!
    #[test]
    fn it_internal_adds_two() {
        assert_eq!(4, internal_adder(2, 2));
    }
}

pub fn add_two(a: i32) -> i32 {
    internal_adder(a, 2)
}

fn internal_adder(a: i32, b: i32) -> i32 {
    a + b
}
