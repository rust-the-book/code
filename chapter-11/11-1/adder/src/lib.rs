#[cfg(test)]
mod tests {
    // use components outside of tests
    use super::*;

    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }

    #[test]
    #[should_panic]
    fn it_fails() {
        panic!("I failed!");
    }

    #[test]
    fn larger_rectangle_can_hold_smaller() {
        let larger = Rectangle { width: 100, height: 90 };
        let smaller = Rectangle { width: 50, height: 50 };

        assert!(larger.can_hold(&smaller));
    }

    #[test]
    fn smaller_rectangle_cannot_hold_larger() {
        let larger = Rectangle { width: 100, height: 90 };
        let smaller = Rectangle { width: 50, height: 50 };

        assert!(!smaller.can_hold(&larger));
    }

    #[test]
    fn it_adds_two() {
        // implemented as ==
        // values need to implement
        // PartialEq - for comparison
        // Debug - to display value(using debug formatting) in case of failure
        assert_eq!(add_two(2), 4);
    }

    #[test]
    fn its_not_four() {
        // implemented as !=
        // values need to implement
        // PartialEq - for comparison
        // Debug - to display value(using debug formatting) in case of failure
        assert_ne!(add_two(3), 4);
    }

    #[test]
    fn greeting_contains_name() {
        let result = greeting("Carol");
        // assert! just says it failed without showing values
        // here we can pass `format` string & `format` args
        // Syntax: assert!(<condition>, <format_string>, <format_args>)
        //         assert_eq!(<left>, <right>, <format_string>, <format_args>)
        assert!(result.contains("Carol"), "Greeting doesn't contain name. Result was {}", result); 
    }

    #[test]
    #[should_panic]
    fn greater_than_100() {
        Guess::new(101);
    }

    #[test]
    // can be less accurate when we know code panicked
    // but not if its actually from our code
    #[should_panic]
    fn lesser_than_1() {
        Guess::new(0);
    }

    #[test]
    // this would make sure that panic came from our code
    #[should_panic(expected="Guess should be greater than 1")]
    fn lesser_than_1_expected() {
        Guess::new(0);
    }

    #[test]
    #[should_panic(expected="Guess should be lesser than 100")]
    fn greater_than_100_expected() {
        Guess::new(101);
    }

    #[test]
    // alternative to assert_eq!
    // can use `?` operator for code that might return `Err`
    // test fails if any `Err` returned from test
    fn it_works_with_Result() -> Result<(), String> {
        if 2 + 2 == 4 {
            Ok(()) 
        }
        else {
            Err("2 + 2 != 4".to_string()) 
        }
    }

    #[test]
    fn ensure_file_present() -> Result<(), std::io::Error> {
        let f = std::fs::File::open("/etc/hosts")?;
        Ok(())
    }
}

// `struct` & `enum` can use derive trait `#[derive(PartialEq, Debug)]`
struct Rectangle {
    width: u32,
    height: u32
}

impl Rectangle {
    fn can_hold(&self, other: &Self) -> bool {
        self.width > other.width && self.height > other.height 
    }
}

fn add_two(a: i32) -> i32 {
    a + 2
}

fn greeting(name: &str) -> String {
    format!("Hello {}!", name)
}

struct Guess {
    value: i32
}

impl Guess {
    fn new(value: i32) -> Guess {
        if value < 1  {
            panic!("Guess should be greater than 1, got {}", value);
        }
        else if value > 100 {
            panic!("Guess should be lesser than 100, got {}", value);
        }

        Guess {
            value 
        }
    }
}
