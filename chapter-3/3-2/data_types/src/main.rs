fn main() {
    let i_8: i8 = -10;
    let u_8: u8 = 10;
    let i_16: i16 = -10;
    let u_16: u16 = 10;
    let i_32: i32 = -10;
    let u_32: u32 = 10;
    let i_64: i64 = -10;
    let u_64: u64 = 10;
    let i_size: isize = -10;
    let u_size: usize = 10;

    let decimal = 92_222;
    let hex = 0xff;
    let oct = 0o77;
    let bin = 0b1111_0000;
    let byte = b'A'; // u8 only!

    let f_64 = 10.0;
    let f_32: f32 = 10.0;

    let True = true;
    let False = false;

    let c = 'z';
    let c = 'ℤ';
    let c = '😻';

    let tuple: (String, i32, f32) = (String::from("rust"), 10, 10.0);
    let (x, y, z) = tuple;
    let tuple: (String, i32, f32) = (String::from("rust"), 10, 10.0);

    println!("x is {}, y is {}, z is {}", tuple.0, y, tuple.2);

    let array = [1, 2, 3, 4, 5];
    let days_of_week = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
    let array: [i32; 5] = [1, 2, 3, 4, 5];
    let array = [10; 5];
}
