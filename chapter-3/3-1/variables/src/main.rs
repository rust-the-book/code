fn main() {
    let mut x = 5;
    println!("x is {}", x);
    x = 10;
    println!("x is {}", x);

    const MAGIC_PI: f32 = 3.14;
    println!("PI is {}", MAGIC_PI);

    let x = 5;
    let x = x + 1;
    let x = x + 2;

    println!("x is {}", x);
}
