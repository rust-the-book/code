fn main() {
    println!("Hello, world!");

    another_function(5, 10);

    let x = 5;

    let y = {
        let z = 10;

        x + z
    };

    let sum = add(10, 20);
}

fn add(x: i32, y: i32) -> i32 {
    if x > y {
        return x + y + 10; 
    }

    x + y
}

fn another_function(x: i32, y: i32) {
    println!("Hello, another function!");
}
