use std::io;

fn main() {
    loop {
        println!("Enter temperature");

        let mut farenheit = String::new();

        io::stdin().read_line(&mut farenheit)
            .expect("Failed to read input");

        let farenheit: f32 = match farenheit.trim().parse() {
            Ok(num) => num,
            Err(_) => {
                println!("Please enter a number!");
                continue;
            }
        };

        let celsius = to_celsius(farenheit);

        println!("{} f is {} c", farenheit, celsius);

        break;
    }
}

fn to_celsius(farenheit: f32) -> f32 {
    5.0/9.0 * (farenheit - 32.0)
}
