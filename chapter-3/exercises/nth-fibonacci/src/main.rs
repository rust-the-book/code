use std::io;

fn main() {
    loop {
        println!("Enter number!");

        let mut n = String::new();

        io::stdin().read_line(&mut n)
            .expect("error reading input!");
        
        let n: i32 = match n.trim().parse() {
            Ok(num) => num,
            Err(_) => {
                println!("Enter a number!");
                continue;
            }
        };

        let mut n1 = 0;
        let mut n2 = 1;

        for _ in 1..n {
            let n3 = n1 + n2;
            n1 = n2;
            n2 = n3;
        }

        println!("{}th fibonacci is {}", n, n2);
        
        break;
    }
}
