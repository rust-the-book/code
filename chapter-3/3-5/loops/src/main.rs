fn main() {
    let mut counter = 0;

    let result = loop {
        counter += 1;

        if counter == 10 {
            break counter * 2; 
        }
    };

    println!("result is {}", result);

    let mut number = 0;

    while number < 3 {
        println!("number is {}", number);

        number += 1; 
    }
    
    let array = [1, 2, 3, 4, 5];
    let mut index = 0;

    while index < 5 {
        println!("value is {}", array[index]);

        index += 1;
    }

    for value in array.iter() {
        println!("value is {}", value) 
    }

    for i in (1..6).rev() {
        println!("lift off in {}", i); 
    }
}
