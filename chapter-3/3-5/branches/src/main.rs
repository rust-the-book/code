fn main() {
    let number = 3;

    if number > 5 {
        println!("Too big!");
    } else {
        println!("Too small!");
    }

    let number = 6;

    if number % 4 == 0 {
        println!("divisible by 4");
    } else if number % 3 == 0 {
        println!("divisible by 3");
    } else if number % 2 == 0 {
        println!("divisible by 2");
    } else {
        println!("not divisible");
    }

    let number= if number % 3 == 0 {
        number + 1 
    } else {
        number + 2 
    };

    println!("number is {}", number);
}
