// no idea of *concerete values* passed, so `if` or `else` might execute
// no idea of *concrete lifetimes* of values passed
// `borrow checker` has no idea how lifetimes of `x` & `y` relate to `return`
// 
// 'a - generic lifetime, no effect on actual lifetime reference
// says, x, y & return live at least as long as 'a
// return - has concrete lifetime smallest of the passed in references lifetimes
fn longest<'a>(x: &'a str, y: &'a str) -> &'a str {
    if x.len() > y.len() {
        x 
    } else {
        y 
    }
}

// here, y doesn't need lifetime annotation
// since, x is always returned & is enough to annotate it
fn longest2<'a>(x: &'a str, y: &str) -> &'a str {
    x
}

// error
// `lifetime` of `return type` should match `lifetime` of `one of the parameters`
fn longest3<'a>(x: &str, y: &str ) -> &'a str {
    let result = String::from("abcd");

    &result
} // result dropped here!

// `'a` in struct means, `instance` can't outlive `reference` in `part` field
struct ImportantExcerpt<'a> {
    part: &'a str
}

// lifetime annotation here is required
impl<'a> ImportantExcerpt<'a> {
    // in method signatures, reference might be tied to struct fields or be independent
    // 1st elision rule applies
    // &self gets own lifetime annotation
    // return is not a reference
    fn level(&self) -> i32 {
        3 
    }

    // 1st & 3rd elision rule applies
    // &self & announcement gets own lifetime annotation
    // return gets lifetime of &self
    fn announce_and_return_part(&self, announcement: &str) -> &str {
        println!("Announcement: {}", announcement);
        self.part
    }
}

// * why did this work?*
// * `pre 1.0`, rust required `lifetime annotations`
// * since this is verbose, rust compiler included these patterns in `lifetime analysis` called `lifetime elision rules`
// * lifetime on *function* or *method* input params called `input lifetimes`
// * lifetime on *function* or *method* return types is called `output lifetimes`
// * if `no explicit annotation`, rust uses *three* rules,
//     * 1st rule - each `input reference parameter` gets its own lifetime annotation
//     * 2nd rule - if only one `input reference parameter`, that lifetime is assigned to `output`
//     * 3rd rule - if multiple `input lifetime parameters`, one of them is `&self` or `&mut self`, then its lifetime is assigned to `output`
// * `1st rule` applies to `input parameters`, `2nd & 3rd` applies to `output parameters`
//
// * when `1st rule` is applied, we get, `fn first_word<'a> first_word(s: &'s str) -> &str {}`
// * `2nd rule` is applied because its satisfied, we get, `fn first_word<'a> first_word(s: &'s str) -> &'a str {}`
fn first_word(s: &str) -> &str {
    &s[..]
}

// * when `1st rule` is applied, we get, `fn longest<'a, 'b>(x: &'a str, y: &'b str) -> &str {}`
// * `2nd rule` can't be applied, since more than one input parameter
// * `3rd` can't be applied either, since neither of the inputs are `&self` or `&mut self`
// * lifetime of output parameter not found after 2nd & 3rd rule, so we get an error!
fn longest4(x: &str, y: &str) -> &str {
    if x.len() > y.len() {
        x 
    } else {
        y 
    }
}

// `<'a, T>` - lifetime annotation are a type of `generic`, so they go in the same list as `generic type`
fn longest_with_announcement<'a, T>(x: &'a str, y: &'a str, ann: T) -> &'a str
    where T: std::fmt::Display {
    println!("Announcement: {}", ann);

    if x.len() > y.len() {
        x 
    } else {
        y 
    }
}

fn main() {
    let r;
    let x = 5;
    r = &x;

    println!("r: {}", r);

    let x = String::from("abcd");
    let y = "xyz";
    let result = longest(x.as_str(), y);
    println!("longest is {}", result);

    let x = String::from("abcd"); 
    {
        let y = String::from("xyz");
        // has concrete lifetime smallest of x & y lifetime, which is y here
        // result has reference with lifetime of `y`
        let result = longest(x.as_str(), y.as_str());
        println!("longest is {}", result);
    }

    let x = String::from("abcd"); 
    let result;
    {
        let y = String::from("xyz");
        // has concrete lifetime smallest of x & y lifetime, which is y here
        // result has reference with lifetime of `y`
        result = longest(x.as_str(), y.as_str());
    }
    // can't use it here!
    println!("longest is {}", result);

    let novel = String::from("In the year 2020. A deadly pandemic brough the world to a standstill...");
    let first_sentence = novel.split('.')
        .next()
        .expect("Couldn't find '.'");
    let i = ImportantExcerpt { part: first_sentence };

    // special lifetime
    // valid throughout the program
    let s: &'static str = "covid-19 is wreaking havoc!";
} // both r & x are valid here, so reference is valid!
  // both novel & i are valid here
