fn largest<T>(list: &[T]) -> T
    // PartialOrd - allows us to use > opertor, since it provides a default method for it
    // Copy - allows us to do let mut largest = list[0]
    // without Copy, since T is generic, type may or may not implement Copy trait
    // so, the slice is assumed to be non-copiable
    where T: PartialOrd + Copy
{
    let mut largest = list[0];

    for &item in list.iter() {
        // `T` doesn't implement `std::cmp::PartialOrd`
        // `i32` & `char` might implement that
        // but `T` supposedly represents all types
        // who may or may not have implemented it
        if item > largest {
            largest = item;
        }
    }

    largest
}

fn main() {
    let number_list = vec![34, 50, 25, 100, 65];
    let result = largest(&number_list);
    println!("The largest number is {}", result);
    assert_eq!(result, 100);

    let char_list = vec!['y', 'm', 'a', 'q'];
    let result = largest(&char_list);
    println!("The largest char is {}", result);
    assert_eq!(result, 'y');
}
