use std::fmt::Display;

struct Pair<T> {
    x: T,
    y: T
}

impl<T> Pair<T> {
    // for all `T`
    fn new(x: T, y: T) -> Self {
        Self {x, y}
    }
}

impl<T: Display + PartialOrd> Pair<T> {
    // for all `T`, if only if `T` implements `Display` & `PartialOrd`
    fn cmp_display(&self) {
        if self.x > self.y {
            println!("largest value is x = {}", self.x);
        }
        else {
            println!("largest value is y = {}", self.y);
        }
    }
}

trait ToStringCustom {
    fn to_string_c(&self) -> String;
}

// implement trait for types that implement certian trait
// implement `ToStringCustom` for all `T`, if & only if `T` implements `Display` & `ToString`
impl<T: Display+ ToString> ToStringCustom for T {
    fn to_string_c(&self) -> String {
        println!("Custom version of to_string! calling actual one");
        format!("{}", self.to_string())
    }
}

fn main() {
    let x = 10;
    println!("to_string: {}", x.to_string());
    println!("to_string_c: {}", x.to_string_c());

    let x = String::from("Hello");
    println!("to_string: {}", x.to_string());
    println!("to_string_c: {}", x.to_string_c());
}
