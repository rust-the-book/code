trait Summary {
    fn summarize(&self) -> String;
}

trait SummaryDefault {
    fn summarize_author(&self) -> String;

    // can call other trait methods
    fn summarize_default(&self) -> String {
        format!("default summary by {}", self.summarize_author())
    }
}

struct NewsArticle {
    headline: String,
    location: String,
    author: String,
    content: String
}

// protected by `coherence` or `orphan rule` 
// * trait or type should be local to our crate
// * can implement Display trait on NewsArticle type
// * or, can implement Summary trait on Vec<T>
// * can't implement external trait for external type, like Display for Vec<T>
impl Summary for NewsArticle {
    fn summarize(&self) -> String {
        format!("{}, by {} {}", self.headline, self.author, self.location)
    }
}

impl SummaryDefault for NewsArticle {
    // overriding default implementation is same as providing one
    fn summarize_author(&self) -> String {
        format!("{}", self.author) 
    }
}

struct Tweet {
    username: String,
    content: String,
    reply: String,
    retweet: bool
}

impl Summary for Tweet {
    fn summarize(&self) -> String {
        format!("{}: {}", self.username, self.content)
    }
}

impl SummaryDefault for Tweet {
    fn summarize_author(&self) -> String {
        format!("@{}", self.username) 
    }
}

// any `item` as long as `type` implements `Summary` trait
fn notify(item: &impl Summary) {
    println!("Breaking news! {}", item.summarize());
}

// less verbose than trait bound & useful in simple usecases
// syntactic sugar for trait bound syntax
// allows different types to be passed to item1 & item2
// as long as, they implement Summary trait
fn notify_impl_syntax(item1: &impl Summary, item2: &impl Summary) {
    println!("Breaking news! {}, {}", item1.summarize(), item2.summarize());
}

// more verbose
// can force both item1 & item2 of the same type
fn notify_trait_bound<T: Summary>(item1: &T, item2: &T) {
    println!("Breaking news! {}, {}", item1.summarize(), item2.summarize());
}

// + says item1 & item2 should implement both Summary & SummaryDefault trait
fn notify_multiple_traits_impl_syntax(item1: &(impl Summary + SummaryDefault), item2: &(impl Summary + SummaryDefault)) {}

// + says item1 & item2 should implement both Summary & SummaryDefault trait
fn notify_multiple_traits_trait_bound<T: Summary + SummaryDefault>(item1: &T, item2: &T) {}

// where improves readability of trait bound syntax
// function name, args & return type are close together
fn notify_trait_bound_where<T, U>(item1: &T, item2: &U) -> i32
    where T: Summary + SummaryDefault,
          U: Summary + SummaryDefault {
    0
}

// can return any type that implements Summary trait
fn returns_summarizable() -> impl Summary {
    Tweet {
        username: String::from("Rust Bot"),
        content: String::from("Covid-19 is wreaking havoc across the globle"),
        reply: String::from("Indeed!"),
        retweet: true
    }
}

// can return only one type that compiler knows for sure
// not either or, like here, switch value could be known only at runtime
fn returns_any_summarizable(switch: bool) -> impl Summary {
    if switch {
        NewsArticle {
            headline: String::from("Covid-19 is wreaking havoc across the globle"),
            location: String::from("Everywhere"),
            author: String::from("Rust Bot"),
            content: String::from("Covid-19 is wreaking havoc across the globle"),
        }
    }
    else {
        Tweet {
            username: String::from("Rust Bot"),
            content: String::from("Covid-19 is wreaking havoc across the globle"),
            reply: String::from("Indeed!"),
            retweet: true
        }
    }
}

fn main() {
    let news = NewsArticle {
        headline: String::from("Covid-19 is wreaking havoc across the globle"),
        location: String::from("Everywhere"),
        author: String::from("Rust Bot"),
        content: String::from("Covid-19 is wreaking havoc across the globle"),
    };
    println!("{}", news.summarize());
    println!("{}", news.summarize_default());
    notify(&news);

    let tweet = Tweet {
        username: String::from("Rust Bot"),
        content: String::from("Covid-19 is wreaking havoc across the globle"),
        reply: String::from("Indeed!"),
        retweet: true
    };
    println!("{}", tweet.summarize());
    println!("{}", tweet.summarize_default());
    notify(&tweet);

    notify_impl_syntax(&news, &tweet);
    notify_trait_bound(&tweet, &tweet);

    notify_multiple_traits_impl_syntax(&news, &tweet);
    notify_multiple_traits_trait_bound(&news, &news);
}
