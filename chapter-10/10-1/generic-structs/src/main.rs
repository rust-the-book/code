struct Point<T> {
    x: T,
    y: T
}

struct Point2<T, U> {
    x: T,
    y: U
}

fn main() {
    let p1 = Point {x: 5, y: 6};
    let p2 = Point {x: 5.0, y: 6.0};
    // causes error
    // `x` & `y` are of same type `T`
    // but here, one is int, one is float
    let p3 = Point {x: 5, y: 6.0};
    // this works, since `x` & `y` are different generic types
    let p4 = Point2 {x: 5, y: 6.0};
}
