#[derive(Debug)]
enum IOption<T> {
    Some(T),
    None
}

#[derive(Debug)]
enum IResult<T, E> {
    Ok(T),
    Err(E)
}

fn main() {
    let option: IOption<i32> = IOption::Some(10);
    let result: IResult<i32, char> = IResult::Err('n');

    println!("option: {:?}, result: {:?}", option, result);
}
