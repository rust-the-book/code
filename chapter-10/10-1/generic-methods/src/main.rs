struct Point<T> {
    x: T,
    y: T
}

// `T` after `impl` says that `T` is a generic type & not concrete
// and, this method is available for all types of `Point`
impl<T> Point<T> {
    fn x(&self) -> &T {
        &self.x 
    }
}

// No `T` after `impl`? this says that `f32` is of concrete type & not generic
// this method is only applicable for `Point` of type `f32`
impl Point<f32> {
    fn distance_from_origin(&self) -> f32 {
        (self.x.powi(2) + self.y.powi(2)).sqrt()
    }
}

#[derive(Debug)]
struct Point2<T, U> {
    x: T,
    y: U
}

// demonstrates, `T` & `U` are relevant for `Point`
impl<T, U> Point2<T, U> {
    // `V` & `W` relevant only for `mixup` method
    fn mixup<V, W>(self, other: Point2<V, W>) -> Point2<T, W> {
        Point2 {
            x: self.x,
            y: other.y
        } 
    }
}

fn main() {
    let p1 = Point { x: 5, y: 6 };
    println!("p1.x: {}", p1.x());
    //println!("distace from origin: {}", p1.distance_from_origin());
    let p2 = Point { x: 5.0, y: 6.0 };
    println!("distace from origin: {}", p2.distance_from_origin());

    let p1 = Point2 { x: 5, y: 6.0 };
    let p2 = Point2 { x: "Hello", y: 'c' };
    let p3 = p1.mixup(p2);
    println!("p3: {:#?}", p3);
}
