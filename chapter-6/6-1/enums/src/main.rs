enum IpAddrKind {
    V4,
    V6,
}

struct IpAddr {
    kind: IpAddrKind,
    address: String
}

// enum variants can hold type
// grouping ip addr & kind even more concisely than the struct!
enum IpAddress {   
    V4(String),
    V6(String)
}

// enum variants can hold different types
enum IpAddressAnother {
    V4(u8, u8, u8, u8),
    V6(String)
}

#[derive(Debug)] // for printing self!
enum Message {
    Quit,
    Move {x: i32, y: i32},
    Write(String),
    ChangeColor(i32, i32, i32)
}

// enums can have implementations
impl Message {
    fn call(&self) {
        if let Message::Quit = self {
            println!("{:?}", self);
        }

        if let Message::Move{x, y} = self {
            println!("{}, {}", x, y);
        }
        
        if let Message::Write(message) = self {
            println!("{}", message);
        }

        if let Message::ChangeColor(x, y, z) = self {
            println!("{}, {}, {}", x, y, z);
        }
    }
}

// following structs same as Message enum
// but grouping lost
// hard to pass to functions
struct QuitMessage();

struct MoveMessage {
    x: i32,
    y: i32
}

struct WriteMessage(String);

struct ChangeColor(i32, i32, i32);

fn main() {
    let v4 = IpAddrKind::V4;
    let v6 = IpAddrKind::V6;

    route(v4);
    route(v6);

    let home = IpAddr {
        kind: IpAddrKind::V4,
        address: String::from("127.0.0.1")
    };

    let loopback = IpAddr {
        kind: IpAddrKind::V6,
        address: String::from("::1")
    };

    let home = IpAddress::V4(String::from("127.0.0.1"));
    let loopback = IpAddress::V6(String::from("::1"));

    let home = IpAddressAnother::V4(127, 0, 0, 1);
    let loopback = IpAddressAnother::V6(String::from("::1"));

    let message = Message::Quit;
    message.call();

    let message = Message::Move {x: 10, y: 20};
    message.call();

    let message = Message::Write(String::from("hello, world!"));
    message.call();

    let message = Message::ChangeColor(200, 100, 0);
    message.call();

    let some_string = Some("hello");
    let some_number = Some(1);
    let null: Option<i32> = None;
}

fn route(ip_addr: IpAddrKind) {}
