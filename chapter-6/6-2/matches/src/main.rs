enum Coin {
    Penny,
    Nickel,
    Dime,
    Quarter(UsState)
}

#[derive(Debug)]
enum UsState {
    Alabama,
    Alaska
}

fn plus_one(value: Option<i32>) -> Option<i32> {
    match value {
        None => None,
        Some(val) => Some(val + 1)
    }
}

fn value_in_cents(coin: Coin) -> u8 {
    // whole match is an expression
    // so its a valid return
    match coin {
        // code part of the arm is an expression
        // so its return is the result of entire match
        Coin::Penny => {
            println!("Lucky penny!");
            1
        },
        Coin::Nickel => 5,
        Coin::Dime => 10,
        // coin is Coin::Quarter(UsState::Alabama))
        // state binds to UsState::Alabama
        // that's how we extract value
        Coin::Quarter(state) => {
            println!("Quarter belongs to state: {:#?}", state);
            25
        },
    }
}

fn main() {
    println!("{}", value_in_cents(Coin::Dime));
    println!("{}", value_in_cents(Coin::Quarter(UsState::Alabama)));

    let five = Some(5);
    let six = plus_one(five);
    let none = plus_one(None);

    let some_value: u8 = 10;

    // sometimes we can't match all possible values
    // or, we don't want to
    // use _ to catch all
    match some_value {
        1 => println!("1"),
        3 => println!("3"),
        5 => println!("5"),
        7 => println!("7"),
        _ => println!("{}", some_value),
    };
}
