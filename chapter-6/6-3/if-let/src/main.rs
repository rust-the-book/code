fn main() {
    let some_value = Some(3);

    // we only care if value is Some(3)
    // and, ignore other Some(x) or None
    match some_value {
        Some(3) => println!("three!"),
        _ => println!("something else!")
    };

    // same as above, but more concise & natural
    if let Some(3) = some_value {
        println!("three!");
    } else {
        println!("something else!");
    }
}
