// not included in prelude
// least used of all collections :(
use std::collections::HashMap;

fn main() {
    // HashMap<K, V>
    let mut scores = HashMap::new();
    // K & V inferred from insert
    // if not error will be thrown
    scores.insert(String::from("Blue"), 10);
    scores.insert(String::from("Yellow"), 15);

    let teams = vec![String::from("Blue"), String::from("Yellow")];
    let initial_scores = vec![10, 15];
    // can be created from vector of tupes using `collect`
    let scores: HashMap<_, _> = teams.iter().zip(initial_scores.iter()).collect();

    let field_name = String::from("key");
    let field_value = String::from("value");
    let mut hm = HashMap::new();
    // types with `Copy` trait are copied
    // types like `String` are moved
    // can have `reference`, but needs to be valid as long as `hm`
    hm.insert(field_name, field_value);
    // `field_name` and `field_value` invalid here!
    
    let name = String::from("Blue");
    // takes `&K`, here `&String`
    // returns `Option<&V>`, here `Option<&String>`
    let score = scores.get(&name);
    for (k, v) in &hm {
        println!("{}: {}", k, v);
    }

    let mut scores = HashMap::new();
    scores.insert(String::from("Blue"), 40);
    scores.insert(String::from("Yellow"), 50);

    // `entry` special api, returns `Entry`, takes key
    // `or_insert` - if present `insert` & return `mutable ref` of value
    // else, return `mutable ref` of value
    *scores.entry(String::from("Green")).or_insert(50) *= 10;
    let b = scores.entry(String::from("Blue")).or_insert(40);
    *b += 10;
    println!("{:#?}", scores);

    let text = "hello world to wonderful world";
    let mut words = HashMap::new();

    for word in text.split_whitespace() {
        // returns `&mut V`, here `&mut i32`
        let counter = words.entry(word).or_insert(0);
        // `deref` with `*`
        *counter += 1;
    }

    println!("{:#?}", words);
}
