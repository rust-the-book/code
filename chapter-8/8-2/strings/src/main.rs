fn main() {
    let data = "initial contents";

    // from `string literals
    let s = data.to_string();
    let s = "initial contents".to_string();
    let s = String::from(data);
    let s = String::from("initial_contents");
    // from `String ref`
    let s = String::from(&s);
    // from `string slices`
    let s = String::from(&s[..]);

    // Strings are UTF-8 encoded
    let hello = String::from("السلام عليكم");
    let hello = String::from("Dobrý den");
    let hello = String::from("Hello");
    let hello = String::from("שָׁלוֹם");
    let hello = String::from("नमस्ते");
    let hello = String::from("こんにちは");
    let hello = String::from("안녕하세요");
    let hello = String::from("你好");
    let hello = String::from("Olá");
    let hello = String::from("Здравствуйте");
    let hello = String::from("Hola");

    let mut s = String::from("Hello");
    // takes `string slice`
    s.push_str(", World");
    // takes `character`
    s.push('!');

    let s1 = String::from("hello");
    let s2 = String::from("world");
    // roughly, `+` -> `fn add(self, other: &str) -> String {}`
    let s3 = s1 + &s2;

    let s1 = String::from("tic");
    let s2 = String::from("tac");
    let s3 = String::from("toe");
    let s4 = s1 + " " + &s2 + " " + &s3;
    // similar to println!
    // no ownership
    // returns String
    // easy to use and understand
    let s1 = String::from("tic");
    let s2 = String::from("tac");
    let s3 = String::from("toe");
    let s4 = format!("{} {} {}", s1, s2, s3);

    // indexing is common in other languages
    // its an error in rust
    let s = String::from("hello");
    // let first = s[0];
    let len = s.len(); // 5

    // string's are UTF-8 encoded
    // roughly each character could take 2 bytes
    // when we ask s[0], what we expect?
    // 3? byte value? or something else?
    // rust doesn't guess, so its en error
    let s = String::from("Здравствуйте");
    let len = s.len(); // 12? no, 24!

    // 3 ways to look at string
    // byte value - [208, 112,...]
    // scalar value - `வ ி க  க ி ப  ப  ட ி ய ா`
    // graphmeme clusters - `விககிபபடியா"`
    // graphmeme clusters called `diacritics`
    // based on this devs can choose which form they want to work with
    // indexing is supposed to be `O(1)`, but rust has to search the string
    // to make out the structure, so its not allowed
    let wikipedia = "விககிபபடியா";

    let s = String::from("Здравствуйте");
    // here, each char is 2 bytes
    // we would get 3д
    let slice = &s[0..4];
    // this would panic! slice should be at valid UTF-8 boundaries
    let slice = &s[0..1];

    // iterate over chars
    // note: this would not join the grapheme clusters
    // print a tamil word with dots & stuff like above and see for yourself
    for c in s.chars() {
        println!("{}", c);
    }

    for b in s.bytes() {
        println!("{}", b);
    }
}
