fn main() {
    // without initial values, we need to give type Vec holds
    let v: Vec<i32> = Vec::new();
    // macro can infer values based from values given
    let v = vec![1, 2, 3];

    // when data is supposedly inserted later
    // type is inferred
    // if not, error will be thrown
    let mut v = Vec::new();

    v.push(1);
    v.push(2);
    v.push(3);

    {
        let v = vec![1, 2, 3];
    } // `v` dropped here and so are its elements
      // caveat: tricky when elements are references

    let v = vec![1, 2, 3];
    let third: &i32 = &v[2];
    println!("third element is {}", third);

    match v.get(2) {
        Some(third) => println!("third element is {}", third),
        None => println!("no third element")
    };

    // this would panic, useful in `fail fast` scenarios
    // let non_existing = &v[100];
    // this would return None, useful in `fail safe` scenarios
    let non_existing = v.get(100);

    let mut v = vec![1, 2, 3];
    // immutable ref to 1st element
    let first = &v[0];
    // `v` borrowed mutably
    // rust would relocate elements to new location
    // if new element can't be placed next to last element
    // `first` would be pointing to deallocated memory
    v.push(1);
    // ref used later
    // uncommenting it causes error
    //println!("first is {}", first);
    
    let mut v = vec![1, 2, 3];

    // borrow elements immutably
    for i in &v {
        println!("i is {}", i);
    }

    // borrow elements mutably
    for i in &mut v {
        // *i deref the pointer to actual value
        *i += 1; 
    }

    // group different cells into a row using Vec
    let row = vec![
        SpreadsheetCell::Int(10),
        SpreadsheetCell::Float(3.14),
        SpreadsheetCell::Text(String::from("PI"))
    ];
}

// can store variants of different types
// spreadsheel cell can store different values
enum SpreadsheetCell {
    Int(i32),
    Float(f64),
    Text(String)
}
