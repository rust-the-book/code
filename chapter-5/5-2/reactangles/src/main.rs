#[derive(Debug)]
struct Rectangle {
    width: u32,
    height: u32
}

fn main() {
    let width: u32 = 120;
    let height: u32 = 110;

    let area_of_rectangle = area((width, height));

    println!("area {}", area_of_rectangle);

    let rectangle = Rectangle {
        width,
        height
    };

    let area_of_rectangle = area_with_structs(&rectangle);

    println!("area {}", area_of_rectangle);

    println!("rectangle {:#?}", rectangle);

    println!("area {}", rectangle.area());

    let rect1 = Rectangle {width: 30, height: 50};
    let rect2 = Rectangle {width: 10, height: 40};
    let rect3 = Rectangle {width: 60, height: 45};

    println!("can rect1 hold rect2 {}", rect1.can_hold(&rect2));
    println!("can rect1 hold rect3 {}", rect1.can_hold(&rect3));

    let square = Rectangle::square(30);

    let rhombus = Rectangle::rhombus(30, 20);
}

fn area(rectangle: (u32, u32)) -> u32 {
    rectangle.0 * rectangle.1
}

fn area_with_structs(rectangle: &Rectangle) -> u32 {
    rectangle.width * rectangle.height
}

impl Rectangle {
    fn area(&self) -> u32 {
        self.width * self.height 
    }

    fn can_hold(&self, other: &Rectangle) -> bool {
        other.area() < self.area() 
    }

    fn square(size: u32) -> Rectangle {
        Rectangle {width: size, height: size} 
    }
}

impl Rectangle {
    fn rhombus(width: u32, height: u32) -> Rectangle {
        Rectangle {width, height} 
    }
}
