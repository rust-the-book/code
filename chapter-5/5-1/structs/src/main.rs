struct User {
    username: String,
    email: String,
    sign_in_count: u32,
    active: bool
}

struct Color(i32, i32, i32);

struct Point(i32, i32, i32);

struct UnitStruct();

fn main() {
    let mut user1 = User {
        username: String::from("John Doe"),
        email: String::from("john.doe@rust-lang.org"),
        sign_in_count: 1,
        active: true
    };
    
    user1.email = String::from("johndoe@rust-lang.org");

    println!("email is {}", user1.email);

    let user2 = build_user(String::from("rust@rust-lang.org"), String::from("rust"));

    println!("username is {}", user2.email);

    let user3 = User {
        username: String::from("jacob"),
        email: String::from("jacob@rust-lang.org"),
        sign_in_count: user2.sign_in_count,
        active: user2.active
    };

    let user4 = User {
        username: String::from("dante"),
        email: String::from("dante@rust-lang.org"),
        ..user2
    };

    let black = Color(0, 0, 10);
    let origin = Point(0, 10, 0);
    let Point(x, y, z) = origin;

    println!("r {}, g {}, b {}", black.0, black.1, black.2);
    println!("x {}, y {}, z {}", x, y, z);

    let unit_struct = UnitStruct();
}

fn build_user(email: String, username: String) -> User {
    User {
        email,
        username,
        sign_in_count: 1,
        active: true
    }
}
